
# Changelog for Storage Hub Icons Library

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v2.3.0] - 2023-03-30

- ported to git

## [v2.0.0] - 2016-06-26

- ported to LR 6.2

## [v1.0.0] - 2013-01-1

- First release
