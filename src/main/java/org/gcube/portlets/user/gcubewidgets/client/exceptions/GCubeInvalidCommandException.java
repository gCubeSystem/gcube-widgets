package org.gcube.portlets.user.gcubewidgets.client.exceptions;

public class GCubeInvalidCommandException extends RuntimeException  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5352477516616595860L;

	public GCubeInvalidCommandException() {
		super();
	}

	public GCubeInvalidCommandException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public GCubeInvalidCommandException(String arg0) {
		super(arg0);
	}

	public GCubeInvalidCommandException(Throwable arg0) {
		super(arg0);
	}
	
	

}
